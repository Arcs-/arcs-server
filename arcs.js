	'use strict'

	const key = 'AIzaSyDg7HI7Es4U21pU_Pn7Nwio5VXcrKxJIWc'
	const charts = {
		'charts': 'PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI',
		'new_pop': 'PLDcnymzs18LWbmCFUlZie7VsxQ_FIF0_y',
		'new_hiphop': 'PLH6pfBXQXHEBElcVFl-gGewA2OaATF4xL',
		'new_electro': 'PLFPg_IUxqnZNTAbUMEZ76_snWd-ED5en7',
		'new_indie': 'PLSn1U7lJJ1UkPrOvoAb6UVRIbJeygpCma',
		'new_rock': 'PLhd1HyMTk3f5S98HGlByL2eH1T3n6J-bR',
		'new_country': 'PLvLX2y1VZ-tHnQyOqyemaWjZjrJYr8ksp'
	}

	const port = 8080
	const sslport = 1720
	const oneYear = 31557600000

	let express = require('express')
	let compress = require('compression')
	let ytdl = require('ytdl-core')
	let avconv = require('fluent-ffmpeg')
	let request = require('request')
	let shortid = require('shortid')
	let zlib = require('zlib')
	let http = require('http')
	let https = require('https')
	let fs = require('fs')

	let app = express()

	avconv.setFfmpegPath('avconv')


	if (!global.taskCollection) {
		global.taskCollection = {
			'': {
				'id': '',
				'message': '',
				'video': '',
				'status': 'ready',
				'progress': 0,
				'title': '',
				'artist': '',
				'cover': '',
				'duration': 0,
				'stream': null
			}
		}
		global.tasksRunning = 0
		global.tasksDone = 0
	}

	app.use(compress())
	app.use(express.static(__dirname + '/public', { maxAge: oneYear }))
	app.use(function (err, req, res, next) {
		log.error(err)
                res.send('Internal Server Error')
		res.end()
	})

	app.use(function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*')
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		next()
	})

	app.get('/api', function(req, res) {
		res.setHeader('Content-Type', 'application/json')
		res.json({ 'tasksRunning': global.tasksRunning, 'tasksDone': global.tasksDone })
		res.end()
	})

	app.get('/api/charts/:kind?', function(req, res) {
		let kind = req.params.kind
		if (charts[kind] === undefined) {
			res.setHeader('Content-Type', 'application/json')
			res.json([])
			res.end()
		} else {

			let today = new Date()
			if (global.chartsUpDate != '' + today.getDate() + today.getMonth()) {
				console.log('Load new charts (' + today.getDate() + '/' + today.getMonth() + ')')
				updateCharts()
				global.chartsUpDate = '' + today.getDate() + today.getMonth()
			}
			res.sendFile(kind + '.json', { root: __dirname + '/cache/' })

		}
	})

	app.get('/api/prepare/:video?', function(req, res) {
		let video = req.params.video
		if (video === undefined) {
			res.setHeader('Content-Type', 'application/json')
			res.json({ 'status': 'error', 'message': 'Provide a video id!' })
			res.end()
		} else {

			if (!/^[a-zA-Z0-9-_]{11}$/.test(video)) {
				res.setHeader('Content-Type', 'application/json')
				res.json({ 'status': 'error', 'message': 'This is not a valid youtube video ID' })
				res.end()
				return
			}

			let url = 'https://www.youtube.com/watch?v=' + video

			ytdl.getInfo(url, {}, function(err, info) {

				if (err) {
					console.log(err)
					res.setHeader('Content-Type', 'application/json')
					res.json({
						'status': 'error',
						'message': (Object.keys(err)
							.length) ? err : 'This video got blocked'
					})
					res.end()

				} else if (info.length_seconds > 60 * 120) {
					res.setHeader('Content-Type', 'application/json')
					res.json({ 'status': 'error', 'message': 'Video is too long, max. 60 min' })
					res.end()

				} else {

					global.tasksRunning += 1

					let uniqueID = shortid.generate()
					let cleanAttributes = getTitleAndArtist(info.title)
					if (cleanAttributes.artist == '') cleanAttributes.artist = info.content_owner_name || info.author.name || ''

					console.log(nowString() + ' preparing info for ' + '[' + video + ']' + ' ' + cleanAttributes.title + ' - ' + cleanAttributes.artist)

					let bestFormat = info.formats[0]
					for (let i = 1; i < info.formats.length; i++) {
						if (bestFormat.audioBitrate >= 128) break

						let currentFormat = info.formats[i]
						if (currentFormat.audioBitrate > bestFormat.audioBitrate) bestFormat = currentFormat
					}

					let stream = ytdl.downloadFromInfo(info, { 'format': bestFormat })

					global.taskCollection[uniqueID] = {
						'id': uniqueID,
						'message': '',
						'video': video,
						'status': 'ready',
						'progress': 0,
						'title': cleanAttributes.title,
						'artist': cleanAttributes.artist,
						'cover': info.iurlmaxres || info.iurlsd || info.iurlhq || info.thumbnail_url || 'http://arcs.io/default_cover.jpg',
						'duration': info.length_seconds,
						'stream': stream
					}

					setTimeout(function() {
						if (global.taskCollection[uniqueID].status == 'ready') global.tasksRunning -= 1
						delete global.taskCollection[uniqueID]
					}, 5 * 60 * 1000) // keep info for 5 min

					res.setHeader('Content-Type', 'application/json')
					res.json(prettyOutput(uniqueID, false))
					res.end()

				}

				// all responses have been sent at this point
			})

		}


	})

	app.get('/api/proxy/:id?/:action?', function(req, res) {
		let id = req.params.id
		if (id === undefined) {
			res.setHeader('Content-Type', 'application/json')
			res.json({ 'status': 'error', 'message': 'Provide the unique ID!' })
			res.end()
		} else {

			if (global.taskCollection[id] === undefined) {
				res.setHeader('Content-Type', 'application/json')
				res.json({ 'status': 'error', 'message': 'This is not a valied unique ID' })
				res.end()
				return
			}

			let streamRequest = req.params.action
			if (streamRequest == 'stream' && global.taskCollection[id].status == 'ready') {
				global.taskCollection[id].status = 'running'
				startDownload(id, res)
				return
			}

			res.setHeader('Content-Type', 'application/json')
			res.json(prettyOutput(id, true))
			res.end()

		}


	})

	app.get('/api/search/:querry?', function(req, res) {
		let querry = req.params.querry
		if (querry === undefined) {
			res.setHeader('Content-Type', 'application/json')
			res.json({ 'status': 'error', 'message': 'Provide an querry!' })
			res.end()
		} else {

			searchYoutube(querry, function(results) {
				res.setHeader('Content-Type', 'application/json')
				res.json(results)
				res.end()

			})

		}

	})

	http.createServer(app)
		.listen(port, function() {
			console.log('Server listening on port ' + port)
		})

	/*

		let httpsOptions = {
        key: fs.readFileSync('sslcert/atom_unlocked.key', 'utf8'),
        cert: fs.readFileSync('sslcert/atom.ink.pem', 'utf8'),
        ca: [
			fs.readFileSync('sslcert/root.crt', 'utf8'),
            fs.readFileSync('sslcert/1_Intermediate.crt', 'utf8'),

        ]
    }


	https.createServer(httpsOptions, app).listen(sslport, function(){
	  console.log('Secure listening on port ' + sslport)
	})
	*/

	function nowString() {
		let date = new Date
		return '(' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ')'
	}

	function prettyOutput(id, more) {
		let data = global.taskCollection[id]
		if (more) return {
			'id': data.id,
			'message': data.message,
			'status': data.status,
			'progress': data.progress,
			'title': data.title,
			'artist': data.artist,
			'cover': data.cover
		}

		else return {
			'id': data.id,
			'message': data.message,
			'title': data.title,
			'artist': data.artist,
			'cover': data.cover
		}
	}


	function startDownload(id, res) {
		let data = global.taskCollection[id]

		// remove all non ascii characters!!
		res.setHeader('Content-Type', 'application/force-download')
		res.setHeader('Content-Disposition', 'File Transfer')

		res.setHeader('Content-Disposition', 'attachment; filename="' +
			data.artist.replace(/[^\x00-\x7F]/g, '') +
			' - ' +
			data.title.replace(/[^\x00-\x7F]/g, '') +
			'.mp3"')

		let gzipStream = zlib.createGzip()
		gzipStream._flush = zlib.Z_SYNC_FLUSH

		let lastProgress = 0

		new avconv({ source: data.stream })

			.noVideo()
			.toFormat('mp3')

			.addOption('-metadata', 'title=' + data.title)
			.addOption('-metadata', 'artist=' + data.artist)
			.addOption('-metadata', 'album=' + data.artist)
			.addOption('-metadata', 'comment=via arcs.io')

			.addOption('-metadata', 'compatible_brands= ')
			.addOption('-metadata', 'minor_version= ')
			.addOption('-metadata', 'major_brand= ')
			.addOption('-metadata', 'Encoding time= ')

			.on('start', function() {
				console.log(nowString() + ' start ' + '[' + data.video + ']')
			})

			.on('progress', function(progress) {

				let sep = progress.timemark.split('.')
				let newProgress = ((sep[0]) * 100 / data.duration | 0)

				if (newProgress > lastProgress) {
					lastProgress = newProgress
					data.progress = lastProgress
				}

			})

			.on('error', function(err) {
				global.tasksRunning -= 1
				global.tasksDone += 1
				data.status = 'error'
				if (err.message = 'Output stream closed') {
					data.message = 'Download interrupted.'
				} else {
					data.message = err.message
					console.log(nowString() + ' error on ' + '[' + data.video + ']', err)
				}
			})

			.on('end', function() {
				global.tasksRunning -= 1
				global.tasksDone += 1
				data.progress = 100
				data.status = 'done'
				console.log(nowString() + ' done ' + '[' + data.video + ']')
			})

			.pipe(gzipStream.pipe(res), { end: true })

	}

	/*
	Trims spaces and minus
	*/
	function trimPlus(str) {
		return str.replace(/^[\s-]+|[\s-]+$/g, '')
	}

	function getTitleAndArtist(title) {
		//let videoTitle = title.replace(/?[^【?]*】|\[[^\[]*\]|(\(|\[)?(Lyrics|Official Video|Official Audio|Official Music Video|Lyric|Lyric Video|Live|Studio Version|Acoustic Version|Performance Edit|audio|dance video|Sydney Session|Acoustic|Pure Imagination)(\)|\])?|'/gi,'')
		let videoTitle = title.replace(/(\(|\[|\【)[^\)\]\】]+(\)|\]|\】)|'/g, '')
		videoTitle = trimPlus(videoTitle)
		if (videoTitle.indexOf(' by ') != -1) {
			let split = videoTitle.indexOf(' by ')
			return {
				'title': videoTitle.substring(0, split)
					.trim(),
				'artist': videoTitle.substring(split + 4)
					.trim()
			}
		} else {
			let split = videoTitle.indexOf('-')
			return {
				'title': trimPlus(videoTitle.substring(split + 1)),
				'artist': videoTitle.substring(0, split)
					.trim()
			}
		}
	}

	/*
	outube api
	*/
	function updateCharts() {

		for (let key in charts) {
			updateChartByKey(key, charts[key])
		}

	}

	function updateChartByKey(name, listId) {
		request({
			url: 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=' + listId + '&key=' + key,
			json: true
		}, function(error, response, body) {

			if (!error && response.statusCode === 200) {

				let arcsCharts = [],
					tmpObject

				for (let i = 0; i < body.items.length; i++) {
					tmpObject = body.items[i].snippet

					if (tmpObject.title == 'Private video' || !tmpObject.thumbnails) continue

					let cleanAttributes = getTitleAndArtist(tmpObject.title)

					arcsCharts.push({
						id: tmpObject.resourceId.videoId,
						title: cleanAttributes.title,
						artist: cleanAttributes.artist,
						cover: tmpObject.thumbnails.medium.url || tmpObject.thumbnails.default.url
					})
				}

				fs.writeFile('cache/' + name + '.json', JSON.stringify(arcsCharts, null, 4), function(err) {
					if (err) console.log(err)
				})

			} else {
				console.log(error)
			}
		})
	}

	function searchYoutube(term, callback) {
		request({
			url: 'https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=8&q=' + encodeURIComponent(term) + '&key=' + key,
			json: true
		}, function(error, response, body) {

			if (!error && response.statusCode === 200) {

				let arcsResults = [],
					tmpObject

				for (let i = 0; i < body.items.length; i++) {
					tmpObject = body.items[i]

					let cleanAttributes = getTitleAndArtist(tmpObject.snippet.title)

					arcsResults.push({
						id: tmpObject.id.videoId,
						title: cleanAttributes.title,
						artist: cleanAttributes.artist,
						cover: tmpObject.snippet.thumbnails.medium.url || tmpObject.snippet.thumbnails.high.url || tmpObject.snippet.thumbnails.default.url
					})
				}

				return callback(arcsResults)

			} else {
				console.log(error)
				return callback({ 'status': 'error', 'message': error, 'code': response.statusCode })

			}
		})


	}
