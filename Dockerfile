FROM 		node:latest

COPY    arcs /var/www/arcs
WORKDIR /var/www/arcs

RUN apt-get update && apt-get install -y libav-tools

RUN npm install -g pm2
RUN mkdir -p /var/log/pm2

RUN npm install

RUN mkdir cache

EXPOSE 8080

ENTRYPOINT ["pm2", "start", "arcs.js","--name","arcs","--log","/var/log/pm2/pm2.log","--watch","--no-daemon"]

# To build:
# docker build . -t arcs

# To run the image (add -d if you want it to run in the background)
# docker run -d --name arcs -p 8080:8080 arcs
